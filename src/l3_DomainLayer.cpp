#include "hw/l3_DomainLayer.h"

bool FastFoodRestaurant::invariant() const {
  return _name.size() <= MAX_NAME && _price >= MIN_PRICE && _price <= MAX_PRICE && _calories >= MIN_CAL && _calories <= MAX_CAL
	  && _time >= MIN_TIME && _time <= MAX_TIME && _popular >= MIN_POPULAR && _popular <= MAX_POPULAR;
}

FastFoodRestaurant::FastFoodRestaurant(std::string name, int price, int calories, int time, int popular): _name(std::move(name)), _price(price), _calories(calories), _time(time), _popular(popular)  {
  assert(invariant());
}

const std::string& FastFoodRestaurant::getName() const {
  return _name;
}

int FastFoodRestaurant::getPrice() const {
  return _price;
}

int FastFoodRestaurant::getCalories() const {
  return _calories;
}

int FastFoodRestaurant::getTime() const {
  return _time;
}

int FastFoodRestaurant::getPopular() const {
  return _popular;
}

bool FastFoodRestaurant::write(std::ostream& os) {
  writeString(os, _name);
  writeNumber(os, _price);
  writeNumber(os, _calories);
  writeNumber(os, _time);
  writeNumber(os, _popular);
  return os.good();
}

std::shared_ptr<ICollectable> ItemCollector::read(std::istream &is) {
  std::string name = readString(is, MAX_NAME);
  int price = readNumber<int>(is);
  int calories = readNumber<int>(is);
  int time = readNumber<int>(is);
  int popular = readNumber<int>(is);
  return std::make_shared<FastFoodRestaurant>(name, price, calories, time, popular);
}
